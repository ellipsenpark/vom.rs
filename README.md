# vom.rs

Sequence predictors using **Variable-Order Markov Chains**, also known as **Probabilistic Finite Automata**.

This library implements the abovementioned predictor, as described in the seminal paper "The Power of Amnesia" by Dana Ron, Yoram Singer and Naftali Tishby.

The automata can be constructed from rules or inferred from a sample string.

It has been developed as a base for the Mégra live coding language.
