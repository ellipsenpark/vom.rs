use crate::pfa::*;
use std::hash::Hash;

impl<T: Eq + Copy + Hash + std::fmt::Debug> std::fmt::Display for PfaInsertionResult<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut source_str = format!("{:?}", self.source);
        source_str.retain(|c| {
            c != '\"'
                && c != '\''
                && c != '['
                && c != ']'
                && c != '{'
                && c != '}'
                && c != ','
                && c != ' '
                && c != '\\'
        });

        let mut dest_str = format!("{:?}", self.destination);
        dest_str.retain(|c| {
            c != '\"'
                && c != '\''
                && c != '['
                && c != ']'
                && c != '{'
                && c != '}'
                && c != ','
                && c != ' '
                && c != '\\'
        });

        let mut sym_str = format!("{:?}", self.symbol);
        sym_str.retain(|c| {
            c != '\"'
                && c != '\''
                && c != '['
                && c != ']'
                && c != '{'
                && c != '}'
                && c != ','
                && c != ' '
                && c != '\\'
        });

        writeln!(
            f,
            "{} -- [{}] -- [{}] --> {}",
            source_str, sym_str, self.prob, dest_str
        )
    }
}

impl<T: Eq + Copy + Hash + std::fmt::Debug> std::fmt::Display for PfaRemovalResult<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut source_str = format!("{:?}", self.source);
        source_str.retain(|c| {
            c != '\"'
                && c != '\''
                && c != '['
                && c != ']'
                && c != '{'
                && c != '}'
                && c != ','
                && c != ' '
                && c != '\\'
        });

        let mut dest_str = format!("{:?}", self.destination);
        dest_str.retain(|c| {
            c != '\"'
                && c != '\''
                && c != '['
                && c != ']'
                && c != '{'
                && c != '}'
                && c != ','
                && c != ' '
                && c != '\\'
        });

        writeln!(f, "{} XX [{}] XX> {}", source_str, self.prob, dest_str)
    }
}

impl<T: Eq + Copy + Hash + std::fmt::Debug> std::fmt::Display for PfaOperationResult<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        writeln!(f, "++++++ PFA OPERATION RESULT ++++++")?;
        if let Some(sym) = self.added_symbol {
            writeln!(f, "ADDED SYMBOL {sym:?}")?;
        }

        if let Some(sym) = self.template_symbol {
            writeln!(f, "TEMPLATE SYMBOL {sym:?}")?;
        }

        writeln!(f, "=== ADDED TRANSITIONS ===")?;
        for at in self.added_transitions.iter() {
            write!(f, "{at}")?;
        }

        writeln!(f, "=== REMOVED TRANSITIONS ===")?;
        for rt in self.removed_transitions.iter() {
            write!(f, "{rt}")?;
        }

        writeln!(f, "=== ADDED STATES ===")?;
        for adds in self.added_states.iter() {
            let mut adds_str = format!("{adds:?}");
            adds_str.retain(|c| {
                c != '\"'
                    && c != '\''
                    && c != '['
                    && c != ']'
                    && c != '{'
                    && c != '}'
                    && c != ','
                    && c != ' '
                    && c != '\\'
            });
            writeln!(f, "{adds_str}")?;
        }

        writeln!(f, "=== REMOVED STATES ===")?;
        for rems in self.removed_states.iter() {
            let mut rems_str = format!("{rems:?}");
            rems_str.retain(|c| {
                c != '\"'
                    && c != '\''
                    && c != '['
                    && c != ']'
                    && c != '{'
                    && c != '}'
                    && c != ','
                    && c != ' '
                    && c != '\\'
            });
            writeln!(f, "{rems_str}")?;
        }

        writeln!(f, "++++++ END PFA OPERATION RESULT ++++++")
    }
}
