/// methods to modify pfa
pub mod operations;
pub mod pfa;
/// display helpers
pub mod pfa_display;
/// Methods and Structures to construct probabilistic suffix trees,
/// following the paper "The Power Of Amnesia" by Ron, Singer and Tishby (1996)
pub mod pst;
