mod operations;
mod pfa;
mod pst;

use std::fs;
use std::time::Instant;

fn main() {
    let mut pfa = pfa::Pfa::<char>::new();

    let state_1 = vec!['a'];
    let state_2 = vec!['b'];
    let state_3 = vec!['c'];
    let state_4 = vec!['e'];
    let state_5 = vec!['a', 'a'];
    let state_6 = vec!['a', 'a', 'b'];

    pfa.add_state(&state_1);
    pfa.add_state(&state_2);
    pfa.add_state(&state_3);
    pfa.add_state(&state_4);
    pfa.add_state(&state_5);
    pfa.add_state(&state_6);

    pfa.add_state_transition(&state_1, &state_2, 0.3, false);
    pfa.add_state_transition(&state_1, &state_5, 0.3, false);
    pfa.add_state_transition(&state_4, &state_1, 0.3, false);
    pfa.add_state_transition(&state_2, &state_4, 0.1, false);
    pfa.add_state_transition(&state_5, &state_6, 1.0, false);
    pfa.add_state_transition(&state_6, &state_3, 1.0, false);
    pfa.add_state_transition(&state_3, &state_1, 1.0, false);

    pfa.rebuild_pst();

    for _ in 0..10 {
        match pfa.next_transition() {
            Some(t) => print!(" {} ({:?})", t.next_symbol, t.current_state),
            None => print!(" NIL"),
        };
    }

    let data0 = pfa::to_dot(&pfa);
    fs::write("manual_pfa_pre_rep.dot", data0).expect("Unable to write file");

    pfa.repeat_symbol('b', 1.0, 2);

    let data01 = pfa::to_dot(&pfa);
    fs::write("manual_pfa_post_rep.dot", data01).expect("Unable to write file");

    println!(" ---- manual done!");

    let sample = vec![
        'a', 'b', 'a', 'c', 'a', 'b', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'a', 'd', 'd', 'c', 'a',
        'a', 'b', 'a', 'c', 'a', 'b', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'a', 'd', 'd', 'c', 'a',
        'a', 'b', 'a', 'c', 'a', 'b', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'a', 'd', 'd', 'c', 'a',
        'a', 'b', 'a', 'c', 'a', 'b', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'a', 'd', 'd', 'c', 'a',
        'b', 'a', 'c', 'a', 'b', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'a', 'd', 'd', 'c', 'a', 'a',
        'b', 'a', 'c', 'a', 'b', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'a', 'd', 'd', 'c', 'a', 'a',
        'b', 'a', 'c', 'a', 'b', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'a', 'd', 'd', 'c', 'a', 'a',
        'b', 'a', 'c', 'a', 'b', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'a', 'd', 'd', 'c', 'a', 'b',
        'a', 'c', 'a', 'b', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'a', 'd', 'd', 'c', 'a', 'a', 'b',
        'a', 'c', 'a', 'b', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'a', 'd', 'd', 'a', 'b', 'a', 'c',
        'a', 'b', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'a', 'd', 'd', 'c', 'a', 'a', 'b', 'a', 'c',
        'a', 'b', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'a', 'd', 'd', 'c',
    ];

    let now = Instant::now();
    let mut learned_pfa = pfa::Pfa::<char>::learn(sample, 2, 0.01, 20);
    let after_learned = now.elapsed().as_secs();

    println!(" ---- learned in {after_learned} !");

    for _ in 0..10 {
        match learned_pfa.next_transition() {
            Some(t) => print!(" {} ({:?})", t.next_symbol, t.current_state),
            None => print!(" NIL"),
        };
    }

    let data1 = pfa::to_dot(&learned_pfa);
    fs::write("learned_pfa.dot", data1).expect("Unable to write file");

    println!(" ---- learned done!");

    let mut rules = vec![
        pfa::Rule {
            source: "a".chars().collect(),
            symbol: 'b',
            probability: 0.3,
        },
        pfa::Rule {
            source: "a".chars().collect(),
            symbol: 'c',
            probability: 0.3,
        },
        pfa::Rule {
            source: "a".chars().collect(),
            symbol: 'a',
            probability: 0.4,
        },
        pfa::Rule {
            source: "aaaa".chars().collect(),
            symbol: 'd',
            probability: 0.5,
        },
        pfa::Rule {
            source: "d".chars().collect(),
            symbol: 'b',
            probability: 1.0,
        },
        pfa::Rule {
            source: "b".chars().collect(),
            symbol: 'a',
            probability: 1.0,
        },
        pfa::Rule {
            source: "c".chars().collect(),
            symbol: 'a',
            probability: 1.0,
        },
    ];

    let mut inferred_pfa = pfa::Pfa::<char>::infer_from_rules(&mut rules, true);

    for _ in 0..10 {
        match inferred_pfa.next_transition() {
            Some(t) => print!(" {} ({:?})", t.next_symbol, t.current_state),
            None => print!(" NIL"),
        };
    }

    let data2 = pfa::to_dot(&inferred_pfa);
    fs::write("inferred_pfa.dot", data2).expect("Unable to write file");

    let data_pst = pst::to_dot(&inferred_pfa.pst_root.unwrap());
    fs::write("inferred_pst.dot", data_pst).expect("Unable to write file");

    println!(" ---- rules done!");

    let mut rules2 = vec![
        pfa::Rule {
            source: "a".chars().collect(),
            symbol: 'a',
            probability: 0.2,
        },
        pfa::Rule {
            source: "aab".chars().collect(),
            symbol: 'c',
            probability: 1.0,
        },
        pfa::Rule {
            source: "a".chars().collect(),
            symbol: 'b',
            probability: 0.3,
        },
        pfa::Rule {
            source: "a".chars().collect(),
            symbol: 'c',
            probability: 0.2,
        },
        pfa::Rule {
            source: "a".chars().collect(),
            symbol: 'e',
            probability: 0.3,
        },
        pfa::Rule {
            source: "b".chars().collect(),
            symbol: 'c',
            probability: 1.0,
        },
        pfa::Rule {
            source: "c".chars().collect(),
            symbol: 'd',
            probability: 1.0,
        },
        pfa::Rule {
            source: "e".chars().collect(),
            symbol: 'a',
            probability: 1.0,
        },
        pfa::Rule {
            source: "d".chars().collect(),
            symbol: 'a',
            probability: 1.0,
        },
    ];

    let mut inferred_pfa2 = pfa::Pfa::<char>::infer_from_rules(&mut rules2, true);

    let data3 = pfa::to_dot(&inferred_pfa2);
    fs::write("inferred_pfa_pre_rem.dot", data3).expect("Unable to write file");

    inferred_pfa2.remove_symbol('b', false);

    //let data_pst3 = pst::to_dot(&inferred_pfa2.pst_root.unwrap());
    //fs::write("inferred_pst_post_rem.dot", data_pst3).expect("Unable to write file");

    let data4 = pfa::to_dot(&inferred_pfa2);
    fs::write("inferred_pfa_post_rem.dot", data4).expect("Unable to write file");

    let probs = vec![1, 20, 40, 9, 10, 20];
    println!("blurred probs {:?}", operations::blur(probs, 1.0));

    let probs2 = vec![1, 20, 40, 9, 10, 20];
    println!("sharpen probs {probs2:?}");
    println!("sharpen probs {:?}", operations::sharpen(probs2, 0.5));

    let mut rules3 = vec![
        pfa::Rule {
            source: "a".chars().collect(),
            symbol: 'b',
            probability: 0.5,
        },
        pfa::Rule {
            source: "a".chars().collect(),
            symbol: 'a',
            probability: 0.5,
        },
        pfa::Rule {
            source: "b".chars().collect(),
            symbol: 'c',
            probability: 1.0,
        },
        pfa::Rule {
            source: "c".chars().collect(),
            symbol: 'a',
            probability: 1.0,
        },
    ];

    let mut inferred_pfa3 = pfa::Pfa::<char>::infer_from_rules(&mut rules3, true);

    let data4 = pfa::to_dot(&inferred_pfa3);
    fs::write("inferred_pfa_3_pre_add.dot", data4).expect("Unable to write file");

    for _ in 0..12 {
        match inferred_pfa3.next_transition() {
            Some(t) => print!(" {} ({:?})", t.next_symbol, t.current_state),
            None => print!(" NIL"),
        };
    }

    println!("\n-----");
}
